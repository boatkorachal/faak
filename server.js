var express = require('express');
var http = require('http');
var path = require('path');
var _ = require('underscore');
var async = require('async');

var app = express();
var timers = {}; // keep reference to timer, key is faaks._id
var MongoStore = require('connect-mongo')(express); // session store
var conf = {
  db: {
    db: 'socket_v3',
    host: 'localhost',
    collection: 'sessions',
    stringify: false,
  },
  secret: '076ee61d63aa10a125ea872411e433b9'
};

var mongodb = require("mongodb"),
    Db = mongodb.Db,
    Connection = mongodb.Connection,
    MongoServer = mongodb.Server,
	ObjectID = mongodb.ObjectID;

var db = new Db('socket_v3', new MongoServer('localhost', Connection.DEFAULT_PORT, {}));
app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({
    secret: conf.secret,
    store: new MongoStore(conf.db),
    key: 'express.sid',
  }));
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));

});
app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/login', function(req, res){
  if( ! req.session.username){
    res.sendfile(__dirname + '/views/login.html');
  }else{
    res.redirect('/');
  }
});
app.post('/login', function(req, res){
  var username = req.body.username;
  // check duplicate username
  db.collection('sessions', function(err, collection){
    if(err) res.redirect('/login');
    collection.findOne({'session.username': username}, function(err, doc){
      if(doc){
        console.log('no');
        res.redirect('/login');
      }else{
        console.log('yes');
        req.session.username = username;
        res.redirect('/');
      }
    });
  });
});
app.post('/create', function(req, res){
  if( ! req.session.username){
    res.redirect('login');
  }else{
    var title = req.body.title;
    var description = req.body.description;
    var time = req.body.time;
    var username = req.body.username;
    var timeoutDate = new Date();
    timeoutDate.setSeconds(timeoutDate.getSeconds() + time*60);

    // db.collection('faaks', function(err, collection){
    //   collection.save({title: title, description: description, creator: username, time: timeoutDate, completed: false, deleted: false, whoGo: username}, function(err,doc){
    //     var timeLeft = Math.round(( new Date(doc.time) - new Date()));
    //     timeLeft = timeLeft < 0? 0: timeLeft;
    //     timers[doc._id] = setTimeout(function(){
    //       completeFaakFromTimer(doc._id);
    //     }, timeLeft)
    //     res.redirect('/'+doc._id);
    //     io.sockets.emit('faaks:create', doc);
    //   });
    // });
    async.waterfall([
        function(callback){
          db.collection('faaks', callback);
        },
        function(collection, callback){
          collection.save({title: title, description: description, creator: username, time: timeoutDate, completed: false, deleted: false, whoGo: username}, callback);
        },
        function(doc, callback){
          var timeLeft = Math.round(( new Date(doc.time) - new Date()));
          timeLeft = timeLeft < 0? 0: timeLeft;
          timers[doc._id] = setTimeout(function(){
            completeFaakFromTimer(doc._id);
          }, timeLeft)
          res.redirect('/'+doc._id);
          io.sockets.emit('faaks:create', doc);
          callback(null, doc);
        }
      ], function(err, results){
        if(err) console.log(err);
        else console.log(results);
      });
  }
});
app.get('/:id', function(req, res){
	if( ! req.session.username){
		res.redirect('login');
	}else{
		res.sendfile(__dirname + '/views/detail.html');
	}
});
app.get('/', function(req, res){
  if( ! req.session.username){
    res.redirect('/login');
  }else{
    res.sendfile(__dirname + '/views/index.html');
  }
});

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
var io = require('socket.io').listen(server,{ log: false });
var connect = require('connect');
var cookie = require('cookie');

io.set('authorization', function (data, accept) {
  //authorize socket session
  //console.log('authorazation', data);
    if (data.headers.cookie) {
        data.cookie = connect.utils.parseSignedCookies(cookie.parse(decodeURIComponent(data.headers.cookie)),'076ee61d63aa10a125ea872411e433b9');
        data.sessionID = data.cookie['express.sid'];
          db.collection('sessions', function(err, collection){
            collection.findOne({_id: data.sessionID}, function(err, doc){;
              if (err || ! doc){
                accept('Error', false);
              }else{
                data.session = doc.session;
                console.log(data.session.username,'has been accepted');
                accept(null, true);
              }
            });
          });
    } else {
       // if there isn't, turn down the connection with a message
       // and leave the function.
       return accept('No cookie transmitted.', false);
    }
});

io.sockets.on('connection', function (socket) {
	socket.username = socket.handshake.session.username; //store username in socket
	socket.emit('username', socket.username); // for storing username on client
  //console.log(socket.username);	
    socket.on('faak:create', function(data, callback){
    });

    // faak collection fetch
    socket.on('faaks:read', function(data, callback){
      console.log('faaks:read');
      // db.collection('faaks', function(err, collection){
      //       collection.find({completed: false}).toArray(function(err, doc){
      //         if(err) callback('error',null);
      //         else{
      //           callback(null, doc);
      //         }
      //       });
      //     });
      async.waterfall([
          function(callback){
            db.collection('faaks', callback);
          },
          function(collection, callback){
            //not complete or complete today
            var todayMidnight = new Date();
            todayMidnight.setHours(0,0,0,0);
            collection.find({ $or: [{completed: false}, {time: {$gte: todayMidnight}}]}).sort({completed: 1,time: -1}).toArray(callback);
          },
          function(doc, callback){
            callback(null, doc);
          }
        ], function(err, result){
          if(err) console.log(err);
          else callback(null, result);
        });
    });

    // faak model fetch (data._id is the id of wanted model)
    socket.on('faak:read', function(data, callback){
    	console.log('faak:read');
      // db.collection('faaks', function(err, collection){
      //   collection.findOne({_id: new ObjectID(data._id)}, function(err, doc){
      //     if(err){
      //       callback('error',null);
      //     }else{
      //       callback(null, doc);  
      //     }
      //   })
      // });
    	async.waterfall([
          function(callback){
            db.collection('faaks', callback);
          },
          function(collection, callback){
            collection.findOne({_id: new ObjectID(data._id)}, callback);
          },
          function(doc, callback){
            callback(null, doc);
          }
        ], function(err, result){
          if(err) console.log(err);
          else callback(null, result);
        });
    });

    // update each faak model
    socket.on('faak:update', function(data, callback){
      console.log('faak:update',data);
      // check if completed or not
      if(data.completed == false){
        // db.collection('faaks', function(err, collection){
        // if(err) callback('error', null);
        // else{
        //   collection.update({_id: new ObjectID(data._id)}, {$set: {title: data.title, description: data.description, time: new Date(data.time)}});
        //   socket.emit('faak/'+data._id+':update', data);
        //   socket.broadcast.emit('faak/'+data._id+':update', data);
        //   callback(null,data);
        // }
        // });
        async.waterfall([
            function(callback){
              db.collection('faaks', callback);
            },
            function(collection, callback){
              var newDate = new Date(data.time);
              collection.findAndModify({_id: new ObjectID(data._id)}, {}, {$set: {title: data.title, description: data.description, time: newDate}}, {new: true}, function(err, doc){
                  if(err) callback(err);
                  else{
                    socket.emit('faak/'+doc._id+':update', doc);
                    socket.broadcast.emit('faak/'+doc._id+':update', doc);
                    callback(null,doc);
                    //set new timeout
                    var timeLeft = Math.round(( newDate - new Date()));
                    timeLeft = timeLeft < 0? 0: timeLeft;
                    clearTimeout(timers[doc._id]);
                    timers[doc._id] = setTimeout(function(){
                        completeFaakFromTimer(doc._id);
                      }, timeLeft)
                  }
              });
            }
          ], function(err, result){
            if(err) console.log(err);
            else callback(null,result);
          });
      
      }else{ //if completed stop the timer
        // update whoGo
        db.collection('faaks', function(err, collection){
          if(err) callback('error', null);
          else{
            collection.update({_id: new ObjectID(data._id)}, {$set: {whoGo: socket.username, time: new Date()}}, {safe:true}, function(err){
              if(err) console.log(err);
              else completeFaakFromTimer(new ObjectID(data._id));  
            });
          }
        });
      }
    });

    // thing collection fetch (data._id is the _id of parent faak model [hack in backbone.sync])
    socket.on('things:read', function(data, callback){
    	console.log('things:read', data);
      // db.collection('things', function(err, collection){
      //   collection.find({parent: new ObjectID(data._id)}).toArray(function(err, docs){
      //     if(err) callback('error',null)
      //       else{
      //         callback(null, docs);
      //       }
      //   });
      // })

      async.waterfall([
          function(callback){
            db.collection('things', callback);
          },
          function(collection, callback){
            collection.find({parent: new ObjectID(data._id)}).toArray(callback);
          },
          function(docs, callback){
            callback(null, docs);
          },
        ], function(err, results){
          if(err) console.log(err);
          else callback(null, results);
        });
    });

    // create thing
    socket.on('thing:create', function(data, callback){
      console.log('thing:create', data);
      // check if that user really create
      //if(data.username == socket.username){
        db.collection('things', function(err, collection){
          collection.save({
            parent: new ObjectID(data.parent),
            username: data.username,
            title: data.title,
            qty: data.qty,
            description: data.description,
          }, function(err,doc){
            io.sockets.emit('things/'+data.parent+':create', doc);
            callback(null, doc);
          });
        });
      //}else{
      //  callback('error',null);
      //}
    });

    // remove thing
    socket.on('thing:delete', function(data, callback){
      console.log('thing:delete');
      db.collection('things', function(err, collection){
        collection.remove({_id: new ObjectID(data._id)}, function(err, doc){
          if(err) callback('error', null);
          else{
            socket.emit('thing/' + data._id + ':delete', data);
            socket.broadcast.emit('thing/' + data._id + ':delete', data);
            callback(null,data)
          }
        });
      });
    });

    // update thing
    socket.on('thing:update', function(data, callback){
      console.log('thing:update', data);
      db.collection('things', function(err, collection){
        if(err) callback('error', null);
        else{
          collection.update({_id: new ObjectID(data._id)}, {$set: {title: data.title, description: data.description, qty: data.qty}});
          socket.emit('thing/'+data._id+':update', data);
          socket.broadcast.emit('thing/'+data._id+':update', data);
          callback(null,data);
        }
      });
    });
});


db.open(function(err, db){
  // at first time server run, find all pending faaks and register to settimeout
  db.collection('faaks', function(err, collection){
    collection.find({completed: false}).toArray(function(err, docs){
      var now = new Date();
      docs.forEach(function(doc){
        var timeLeft = Math.round(( new Date(doc.time) - now));
        timeLeft = timeLeft < 0? 0: timeLeft;
        timers[doc._id] = setTimeout(function(){
          completeFaakFromTimer(doc._id);
        }, timeLeft);
      });
    });
  });
});

function completeFaakFromTimer(id){
  db.collection('faaks', function(err, collection){
    collection.findAndModify({_id: id}, {}, {$set: {completed: true}}, {'new': true}, function(err,doc){ 
        io.sockets.emit('faak/'+doc._id+':update', doc);
        clearTimeout(timers[doc._id]);
    });
  });
}

console.log(timers);


