var app = app || {};
app.page = 'detail';
var ENTER_KEY = 13;
var ARROW_UP_KEY = 38;
var ARROW_DOWN_KEY = 40;
var socket = io.connect();
var username;
var faakId = window.location.pathname.replace('/','');

socket.on('connect', function(){
  socket.once('username', function(username){
    window.username = username;
    
    $(document).ready(function(){
		app._Faak = app._Faak || new app.Faak({_id: window.faakId});
		app._Things = app._Things || new app.Things({_id: window.faakId});

		app._WishList = app._WishList || new app.Things({_id: '000000000000000000000000'});

		app._AppDetailView = app._AppDetailView || new app.AppDetailView();
		
		app._Faak.fetch();
		app._Things.fetch();
		app._WishList.fetch()

	});
  });
});

