var app = app || {};

(function() {
	'use strict';

	// Todo Model
	// ----------

	// Our basic **Todo** model has `title`, `order`, and `completed` attributes.
	app.Thing = Backbone.Model.extend({
		idAttribute: "_id",
		urlRoot: 'thing',
		noIoBind: false,
		socket: window.socket,
		initialize: function(){
			_.bindAll(this, 'serverChange', 'serverDelete', 'modelCleanup');

			if( ! this.noIoBind){
				this.ioBind('update', this.serverChange, this);
				this.ioBind('delete', this.serverDelete, this);
			}
		},
		serverChange: function(data){
			data.fromServer = true;
			this.set(data);
		},
		serverDelete: function(data){
			if(this.collection){
				this.collection.remove(this);
			}else{
				this.trigger('remove',this);
			}
			this.modelCleanup();
		},
		modelCleanup: function(){
			this.ioUnbindAll();
			return this;
		},

	});

}());
