var app = app || {};

$(function($) {
	'use strict';

	app.FaakItemView = Backbone.View.extend({

		tagName: 'tr',
		className: 'clickable',

		template: _.template( $('#faaks-template').html()),

		events: {
			'click': 'rowClicked',
		},

		initialize: function() {
			this.model.on('change:timeLeft', this.renderTimeLeft, this);
			this.model.on('change:title change:description change:time', this.render, this);
			this.model.on('destroy', this.remove, this);
			this.model.on('add', this.render, this);
			this.model.on('change:completed', this.timeoutCompleted, this);
			this.model.on('remove', this.remove, this);
		},

		render: function() {
			var timeLeft = Math.round(( new Date(this.model.get('time')) - new Date() )/1000);
			timeLeft = timeLeft < 0? 0: timeLeft;
			this.model.set({timeLeft: timeLeft});
			this.$el.html( this.template( this.model.attributes ) );
			if(this.model.get('completed')){
				this.$el.removeClass('info').addClass('error');
			}else{
				this.$el.removeClass('error').addClass('info');
			}
			this.timingStart();
			return this;
		},
		timingStart: function(){
			clearInterval(this.counterTimeLeft);
			var self = this;
			this.renderTimeLeft();
			this.counterTimeLeft = setInterval(function(){
				if(self.model.get('timeLeft') <= 0) clearInterval(self.counterTimeLeft);
				else{
					self.model.set({timeLeft: self.model.get('timeLeft') - 1});
				}
			},1000);
		},

		// render only timeleft (span id timeleft)
		renderTimeLeft: function(){
			var timeLeft = this.model.get('timeLeft');
			var minutesLeft = Math.floor(timeLeft / 60);
			var secondLeft = timeLeft % 60;
			var timeLeftString = (minutesLeft > 0)? minutesLeft + ' minute(s) ' : '';
			timeLeftString += secondLeft + ' second(s)';
			this.$("#timeleft").html(timeLeftString);
		},
		timeoutCompleted: function(){
			this.render();
		},
		rowClicked: function(e){
			window.location.href = '/' + this.model.get('_id');
		}

	});
});
