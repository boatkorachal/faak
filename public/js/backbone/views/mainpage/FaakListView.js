var app = app || {};

$(function( $ ) {
	'use strict';

	app.FaakListView = Backbone.View.extend({
		el: '#faaks-list',

		events: {
			
		},

		initialize: function() {
 			this.collection.on('add', this.addToFirst, this);
 			this.collection.on('reset', this.addAll, this);
		},

		render: function() {

		},

		addOne: function( faak ) {
			var view = new app.FaakItemView({model: faak});
			this.$el.append( view.render().el );
		},

		addAll: function() {
			this.$el.html('');
			this.collection.each(this.addOne, this);
		},
		addToFirst: function(faak){
			var view = new app.FaakItemView({model: faak});
			this.$el.prepend( view.render().el );	
		}


	});
});
