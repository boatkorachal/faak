var app = app || {};

$(function( $ ) {
	'use strict';

	app.HeaderMenuView = Backbone.View.extend({

		el: '#header-menu',

		headerTemplate: _.template( $('#header-template').html()),
		events: {
			'click #inc-time': 'increaseTime',
			'click #dec-time': 'decreaseTime',
	    },
		initialize: function() {
			this.$time = this.$("#time");
		},
		render: function () {
	      	$(this.el).html(this.headerTemplate({username: window.username}));
	      	this.$(".positive-integer").numeric({ decimal: false, negative: false });
	      	this.$time = this.$("#time");
	    },
	    increaseTime: function(){
	    	this.$time.val(+this.$time.val() + 1);
	    },
	    decreaseTime: function(){
	    	var tmpTime = +this.$time.val() - 1;
	    	this.$time.val(tmpTime < 1? 1 : tmpTime);
	    }
	});
});
