var app = app || {};

$(function( $ ) {
	'use strict';
	app.FaakHeadView = Backbone.View.extend({
		el: '#faak-head-view',
		template: _.template( $('#faak-head-view-template').html()),
		events: {
			'keypress #inputTitle': 'changeOnEnterTitle',
			'keypress #inputDescription': 'changeOnEnterDescription',
			'click #btnSuccess': 'btnSuccessClicked',
			'click #icon-edit-title': 'editTitle',
			'click #icon-edit-description': 'editDescription',
			'click #btnAddMoreTime': 'addMoreTime',
		},

		initialize: function(){
			this.titleEditing = false;
			this.descriptionEditing = false;
			this.model.on('change:title change:description change:time',this.render,this);
			this.model.on('change:timeLeft',this.renderTimeLeft,this);
			this.model.on('change:completed', this.render, this);
			this.model.on('reset', this.render, this);
		},
		render: function(){
			this.$el.html(this.template(this.model.attributes));
			if(this.titleEditing){
				this.renderTitle('edit');
			}else{
				this.renderTitle('view');
			}
			if(this.descriptionEditing){
				this.renderDescription('edit');
			}else{
				this.renderDescription('view');
			}

			// everybody can click success btn and edit the title

			// if(this.model.get('creator') != window.username){
			// 	this.undelegateEvents();
			// 	this.$("#btnSuccess").hide();
			// 	this.$("#icon-edit-title").hide();
			// 	this.$("#icon-edit-description").hide();
			// }
			if(this.model.get('completed')){
				this.undelegateEvents();
				this.$("#icon-edit-title").hide();
				this.$("#icon-edit-description").hide();
				this.$("#btnAddMoreTime").hide();
				this.$("#btnSuccess").html(this.model.get('whoGo'));
			}

			var timeLeft = Math.round(( new Date(this.model.get('time')) - new Date() )/1000);
			timeLeft = timeLeft < 0? 0: (this.model.get('completed'))? 0: timeLeft;
			this.model.set({timeLeft: timeLeft});
			
			this.timingStart();
			return this;
		},
		renderTitle: function(edit){
			if(edit == 'edit'){
				this.$("#icon-edit-title").removeClass('icon-edit');
				this.$("#icon-edit-title").addClass('icon-ok');
				this.$("#viewTitle").addClass('hide');
				this.$("#editTitle").removeClass('hide');
			}else{
				this.$("#icon-edit-title").removeClass('icon-ok');
				this.$("#icon-edit-title").addClass('icon-edit');
				this.$("#editTitle").addClass('hide');
				this.$("#viewTitle").removeClass('hide');
			}
		},
		renderDescription: function(edit){
			if(edit == 'edit'){
				this.$("#icon-edit-description").removeClass('icon-edit');
				this.$("#icon-edit-description").addClass('icon-ok');
				this.$("#viewDescription").addClass('hide');
				this.$("#editDescription").removeClass('hide');
			}else{
				this.$("#icon-edit-description").removeClass('icon-ok');
				this.$("#icon-edit-description").addClass('icon-edit');
				this.$("#editDescription").addClass('hide');
				this.$("#viewDescription").removeClass('hide');
			}
		},
		changeOnEnterTitle: function(e){
			if ( e && e.which !== ENTER_KEY ) {
				return;
			}
			this.model.save({
				title: this.$("#inputTitle").val().trim(),
				description: this.$("#inputDescription").val().trim(),
			});
			this.titleEditing = false;
			// force render if model wasnt changed
			this.renderTitle('view');
		},
		changeOnEnterDescription: function(e){
			if ( e && e.which !== ENTER_KEY ) {
				return;
			}
			this.model.save({
				title: this.$("#inputTitle").val().trim(),
				description: this.$("#inputDescription").val().trim(),
			});
			this.descriptionEditing = false;
			// force render if model wasnt changed
			this.renderDescription('view');
		},
		btnSuccessClicked: function(){
			this.model.save({
				completed: true,
			},{wait: true});
		},
		timingStart: function(){
			clearInterval(this.counterTimeLeft);
			var self = this;
			this.renderTimeLeft();
			this.counterTimeLeft = setInterval(function(){
				if(self.model.get('timeLeft') <= 0) clearInterval(self.counterTimeLeft);
				else{
					self.model.set({timeLeft: self.model.get('timeLeft') - 1});
				}
			},1000);
		},
		renderTimeLeft: function(){
			var timeLeft = this.model.get('timeLeft');
			var timeLeftString = "";
			if(timeLeft > 0){
				var minutesLeft = Math.floor(timeLeft / 60);
				var secondLeft = timeLeft % 60;
				timeLeftString = (minutesLeft > 0)? minutesLeft + ' minute(s) ' : '';
				timeLeftString += secondLeft + ' second(s)';
			}else{
				timeLeftString = 'Timeout !!';
			}
			this.$("#timeleft").html(timeLeftString);
		},
		editTitle: function(e){
			if(this.titleEditing){
				// save
				this.titleEditing = false;
				this.model.save({
					title: this.$("#inputTitle").val().trim(),
					description: this.$("#inputDescription").val().trim(),
				});
				this.renderTitle('view');
			}else{
				this.titleEditing = true;
				this.$("#viewTitle").addClass('hide');
				this.$("#editTitle").removeClass('hide').children(":first").focus().select();	
				this.renderTitle('edit');
			}
			
		},
		editDescription: function(e){
			if(this.descriptionEditing){
				//save
				this.descriptionEditing = false;
				this.model.save({
					title: this.$("#inputTitle").val().trim(),
					description: this.$("#inputDescription").val().trim(),
				});
				this.renderDescription('view');
			}else{
				this.descriptionEditing = true;
				this.$("#viewDescription").addClass('hide');
				this.$("#editDescription").removeClass('hide').children(":first").focus().select();	
				this.renderDescription('edit');
			}
		},
		addMoreTime: function(){

			var endDate = new Date(this.model.get('time'));
			endDate.setMinutes(endDate.getMinutes() + 1);
			this.model.save({
				title: this.$("#inputTitle").val().trim(),
				description: this.$("#inputDescription").val().trim(),
				time: endDate
			})
		}
	});	

});