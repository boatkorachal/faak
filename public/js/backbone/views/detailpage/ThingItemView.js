var app = app || {};

$(function( $ ) {
	'use strict';
	app.ThingItemView = Backbone.View.extend({
		tagName: "tr",
		template: _.template($("#thing-item-view-template").html()),
		events: {
			'click #btnDeleteItem': 'btnDeleteItemClicked',
			'keypress #inputTitle': 'changeOnEnterTitle',
			'keypress #inputDescription': 'changeOnEnterDescription',
			'click #icon-edit-title': 'editTitle',
			'click #icon-edit-description': 'editDescription',
			'click #dec-qty': 'editQty',
			'click #inc-qty': 'editQty',
		},
		initialize: function(){
			this.titleEditing = false;
			this.descriptionEditing = false;
			_.bindAll(this, 'btnDeleteItemClicked');
			this.model.on('remove', this.removeThisItem, this);
			this.model.on('change', this.render, this);
			if(app.page == 'detail'){ // check if in detailview because this is also used in mainview
				app._Faak.on('change:completed', this.render, this);	
			}
			
		},
		render: function(){
			this.$el.html(this.template({
									title: this.model.get('title'),
									description: this.model.get('description'),
									qty: this.model.get('qty'),
								}));

			if(this.titleEditing){
				this.renderTitle('edit');
			}else{
				this.renderTitle('view');
			}
			if(this.descriptionEditing){
				this.renderDescription('edit');
			}else{
				this.renderDescription('view');
			}

			if(this.model.get('username') != window.username){
				this.undelegateEvents();
				this.$("#btnDeleteItem").hide();
				this.$("#icon-edit-title").hide();
				this.$("#icon-edit-description").hide();
				//qty chooser hide if now owner
				this.$("#editQty").hide();
			}else{
				this.$("#viewQty").hide();
			}

			// if only in detail view 
			if(app.page == 'detail' && app._Faak.get('completed')){
				//show only view not edit
				this.$("#editQty").hide();
				this.$("#viewQty").show();

				//not show edit icons
				this.$("#icon-edit-title").hide();
				this.$("#icon-edit-description").hide();

				// whoever is going to buy things has potential to delete things
				if(app._Faak.get('whoGo') == window.username){
					this.delegateEvents();
					this.$("#btnDeleteItem").show();
				}else{
					this.undelegateEvents();
					this.$("#btnDeleteItem").hide();	
				}
				
			}
			this.$(".positive-integer").numeric({ decimal: false, negative: false });
			return this;
		},
		renderTitle: function(edit){
			if(edit == 'edit'){
				this.$("#icon-edit-title").removeClass('icon-edit');
				this.$("#icon-edit-title").addClass('icon-ok');
				this.$("#viewTitle").addClass('hide');
				this.$("#editTitle").removeClass('hide');
			}else{
				this.$("#icon-edit-title").removeClass('icon-ok');
				this.$("#icon-edit-title").addClass('icon-edit');
				this.$("#editTitle").addClass('hide');
				this.$("#viewTitle").removeClass('hide');
			}
		},
		renderDescription: function(edit){
			if(edit == 'edit'){
				this.$("#icon-edit-description").removeClass('icon-edit');
				this.$("#icon-edit-description").addClass('icon-ok');
				this.$("#viewDescription").addClass('hide');
				this.$("#editDescription").removeClass('hide');
			}else{
				this.$("#icon-edit-description").removeClass('icon-ok');
				this.$("#icon-edit-description").addClass('icon-edit');
				this.$("#editDescription").addClass('hide');
				this.$("#viewDescription").removeClass('hide');
			}
		},
		btnDeleteItemClicked: function(){
			this.model.destroy({silent: true, wait: true});
		},
		removeThisItem: function(thing){
			this.undelegateEvents(); //???? necessary?
			this.remove();
			var thisModelId = thing.get('_id');
			this.options.parentView.itemRemoved(thisModelId);
		},
		changeOnEnterTitle: function(e){
			if ( e.which !== ENTER_KEY ) {
				return;
			}
			this.model.save({
				title: this.$("#inputTitle").val().trim(),
				description: this.$("#inputDescription").val().trim(),
				qty: this.$("#inputQty").val().trim(),
			});
			this.titleEditing = false;
			this.renderTitle('view');
		},
		changeOnEnterDescription: function(e){
			if ( e.which !== ENTER_KEY ) {
				return;
			}
			this.model.save({
				title: this.$("#inputTitle").val().trim(),
				description: this.$("#inputDescription").val().trim(),
				qty: this.$("#inputQty").val().trim(),
			});
			this.descriptionEditing = false;
			this.renderDescription('view');
		},
		editTitle: function(e){
			if(this.titleEditing){
				// save
				this.titleEditing = false;
				this.model.save({
					title: this.$("#inputTitle").val().trim(),
					description: this.$("#inputDescription").val().trim(),
					qty: this.$("#inputQty").val().trim(),
				});
				this.renderTitle('view');
			}else{
				this.titleEditing = true;
				this.$("#viewTitle").addClass('hide');
				this.$("#editTitle").removeClass('hide').children(":first").focus().select();	
				this.renderTitle('edit');
			}
			
		},
		editDescription: function(e){
			if(this.descriptionEditing){
				//save
				this.descriptionEditing = false;
				this.model.save({
					title: this.$("#inputTitle").val().trim(),
					description: this.$("#inputDescription").val().trim(),
					qty: this.$("#inputQty").val().trim(),
				});
				this.renderDescription('view');
			}else{
				this.descriptionEditing = true;
				this.$("#viewDescription").addClass('hide');
				this.$("#editDescription").removeClass('hide').children(":first").focus().select();	
				this.renderDescription('edit');
			}
		},
		editQty: function(e){
			var inputQty = this.$("#inputQty");

			if(e.currentTarget.id == 'dec-qty'){
				var tmpQty = +inputQty.val() - 1;
				inputQty.val(tmpQty < 1? 1 : tmpQty);
			}else if(e.currentTarget.id == 'inc-qty'){
				inputQty.val(+inputQty.val() + 1);
			}

			this.model.save({
				title: this.$("#inputTitle").val().trim(),
				description: this.$("#inputDescription").val().trim(),
				qty: this.$("#inputQty").val().trim(),
			});

		},

	});



});