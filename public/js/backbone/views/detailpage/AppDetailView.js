var app = app || {};

$(function( $ ) {
	'use strict';
	
	app.AppDetailView = Backbone.View.extend({
		el: '#app-container',
		
		events: {
			
		},

		initialize: function() {
			this.headerMenuView = new app.HeaderMenuView();
			this.faakHeadView = new app.FaakHeadView({model: app._Faak});
			this.thingAddView = new app.ThingAddView();
			this.thingGroupListView = new app.ThingGroupListView({collection: app._Things});
			this.thingSummaryView = new app.ThingSummaryView({collection: app._Things});
			this.wishListListView = new app.WishListListView({collection: app._WishList});
			this.render();
		},

		render: function() {
			this.headerMenuView.render();
			return this;
		},
		addAll: function(){
			
		},

	});
});
