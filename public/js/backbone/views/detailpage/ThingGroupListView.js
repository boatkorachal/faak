var app = app || {};

$(function( $ ) {

	app.ThingGroupListView = Backbone.View.extend({
		el: '#thing-group-list-view',
		initialize: function(){
			this.collection.on('reset', this.addAllGroupListView, this);
			this.collection.on('add', this.addThingFromServer, this);
		},
		render: function(){
		},
		addAllGroupListView: function(){
			var groupOfThingByUsername = {};
			var self = this;
			this.collection.each(function(thing, idx){
				var tmpUsername = thing.get('username');
				var tmpId = thing.get('_id');
				if( ! groupOfThingByUsername[tmpUsername]){
					groupOfThingByUsername[tmpUsername] = {};
				}
				groupOfThingByUsername[tmpUsername][tmpId] = thing;
			});
			this.groupOfThingListView = {};
			this.$el.html('');
			_.each(groupOfThingByUsername, function(thingsGroup, usernameKey){
				// create $el for list view and append to #faak-detail-list-item
				var el = self.make("div", {"id": usernameKey, "class": "well"});
				self.groupOfThingListView[usernameKey] = new app.ThingListView({ username: usernameKey,thingsGroup: thingsGroup, el: el, parentView: self});
				if(usernameKey == window.username){
					// show first
					self.$el.prepend(el);
				}else{
					self.$el.append(el);	
				}
				self.groupOfThingListView[usernameKey].addAllThingsGroup();
			});

		},
		addThingFromServer: function(thing){
			// if existingGroupView
			if(this.groupOfThingListView[thing.get('username')]){
				this.groupOfThingListView[thing.get('username')].addThingToGroup(thing);
			}else{
				var el = this.make("div", {"id": thing.get('username'), "class": "well"});
				var thingObj = {};
				thingObj[thing.get('_id')] = thing;
				this.groupOfThingListView[thing.get('username')] = new app.ThingListView({ username: thing.get('username'),thingsGroup: thingObj, el: el, parentView: this});
				if(thing.get('username') == window.username){
					this.$el.prepend(el);
				}else{
					this.$el.append(el);
				}
				this.groupOfThingListView[thing.get('username')].addAllThingsGroup();
			}
		},
		thingListViewRemoved: function(removedUsername){
			// delete for calculation of keys length
			delete(this.groupOfThingListView[removedUsername]);
		}

	});
});