var app = app || {};

$(function( $ ) {
	'use strict';
	
	app.ThingSummaryView = Backbone.View.extend({
		el: '#thing-summary-view',
		events: {
			'click li.clickable': 'copyToAddThing'
		},
		initialize: function(){
			this.collection.on('all', this.render, this);
		},
		render: function(){
			this.$el.html('');
			var groupThings = {};
			this.collection.each(function(thing, idx){
				var item = thing.get('title');
				var qty = thing.get('qty');
				if( ! groupThings[item]) groupThings[item] = 0;
				groupThings[item] += +qty;
			});
			var self = this;
			var documentFragment = document.createDocumentFragment();
			if(Object.keys(groupThings).length){
				_.each(groupThings, function(qty, name){
					documentFragment.appendChild(self.make('li', {class: 'clickable'}, name + ' : ' + qty));
					// self.$el.append(self.make('li', {class: 'clickable'}, name + ' : ' + qty));
				});
				self.$el.html(documentFragment);
			}else{
				self.$el.append(self.make('li', {}, 'no item'));
			}
		},
		copyToAddThing: function(e){
			var itemClicked = e.currentTarget.innerHTML;
			// get item name
			var itemName = itemClicked.split(':');
			itemName.pop();
			itemName = itemName.join(':').trim();

			if(app._AppDetailView){
				app._AppDetailView.thingAddView.$("#inputTitle").val(itemName).focus();	
			}else if(app._AppMainView){
				app._AppMainView.wishListAddView.$("#inputTitle").val(itemName).focus();
			}
			
		}
	});

});