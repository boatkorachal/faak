var app = app || {};

$(function( $ ) {
	'use strict';
	app.ThingListView = Backbone.View.extend({
		// models live in this.options.thingsGroup
		// username live in this.options.username
		initialize: function(){
			this.thingItemViews = {};
			var randomHexColor = "#"+((1<<24)*Math.random()|0).toString(16);
			this.$pTagUsername = $(this.make('p', {class: 'lead'}, this.options.username));
			this.$pTagUsername.css({'border-bottom': '2px solid '+randomHexColor});
			this.$el.append(this.$pTagUsername);
			this.$divThingItemViews = $(this.make('table', {class: 'table table-condensed'}));
			this.$el.append(this.$divThingItemViews);
		},
		render: function(){
		},
		addAllThingsGroup: function(){

			var self = this;
			_.each(this.options.thingsGroup, function(thing, idKey){
				self.thingItemViews[idKey] = new app.ThingItemView({model: thing, parentView: self});
				self.$divThingItemViews.append(self.thingItemViews[idKey].render().$el);
			});
		},
		addThingToGroup: function(thing){
			this.thingItemViews[thing.get('_id')] = new app.ThingItemView({model: thing, parentView: this});
			this.$divThingItemViews.append(this.thingItemViews[thing.get('_id')].render().$el);
		},
		itemRemoved: function(removedId){
			// delete for calculation of keys length
			delete(this.thingItemViews[removedId]);
			// if empty
			if(Object.keys(this.thingItemViews).length <= 0){
				this.remove();
				this.options.parentView.thingListViewRemoved(this.options.username);
			}
		},
	});



});