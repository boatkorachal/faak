var app = app || {};

$(function( $ ) {
	'use strict';

	app.WishListListView = Backbone.View.extend({
		el: '#wishlist-table-view',

		events: {
			'click li.clickable': 'moveToAddThing'
		},

		initialize: function() {
			this.collection.on('all', this.render, this);
		},

		render: function() {

			var groupWishList = {};
			var self = this;
			this.collection.each(function(wishItem, idx){
				var wishItemName = wishItem.get('title');
				var wishItemQty = wishItem.get('qty');
				if( ! groupWishList[wishItemName]) groupWishList[wishItemName] = 0;
				groupWishList[wishItemName] += +wishItemQty;
			});

			var self = this;
			var documentFragment = document.createDocumentFragment();
			_.each(groupWishList, function(qty, name){
				documentFragment.appendChild(self.make('li', {class: 'clickable', 'id': name}, name + ' : ' + qty));
				// self.$el.append(self.make('li', {class: 'clickable'}, name + ' : ' + qty));
			});
			self.$el.html(documentFragment);

			if(app._Faak.get('completed')){
				this.undelegateEvents();
			}

		},

		moveToAddThing: function(e){
			var itemName = e.currentTarget.id;
			this.collection.each(function(wishItem, idx){
				var wishItemName = wishItem.get('title');
				if(itemName == wishItemName){
					//remove from collection
					wishItem.destroy({silent: true, wait: true, success: function(){
						//confirm that it deleted
						//add to things
						var ThingModelClient = app.Thing.extend({ noIoBind: true });
						var addingThing = new ThingModelClient({
							parent: window.faakId,
							title: wishItem.get('title'),
							qty: wishItem.get('qty'),
							description: wishItem.get('description'),
							username: wishItem.get('username'),
						});
						addingThing.save();
					}});
				}
			});
		}

	});
});
