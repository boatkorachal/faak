var app = app || {};

$(function( $ ) {
	'use strict';
	
	app.ThingAddView = Backbone.View.extend({
		el: '#thing-add-view',
		template: _.template($("#thing-add-view-template").html()),
		events: {
			'click #btnSave': 'btnSaveClicked',
			'click #inc-qty': 'increaseQty',
			'click #dec-qty': 'decreaseQty',
			'keypress fieldset': 'onKeyPressed',
			'keydown fieldset': 'onKeyDown'
		},
		initialize: function(){
			_.bindAll(this,'btnSaveClicked');
			app._Faak.on('reset', this.render, this);
			app._Faak.on('change:completed', this.render, this);
		},
		render: function(){
			if(app._Faak.get('completed')){
				this.remove();
			}else{
				this.$el.html(this.template({username: window.username}));
				if(app._Faak.get('completed')){
					this.$(":input").attr('disabled', 'disabled');
				}
			}
			this.$(".positive-integer").numeric({ decimal: false, negative: false });
		},
		btnSaveClicked: function(){

			var itemTitle = this.$("#inputTitle").val().trim();
			var itemQty = this.$("#inputQty").val().trim();
			var itemDescription = this.$("#inputDescription").val().trim();

			if( ! itemTitle || ! itemQty){
				this.$(".icon-exclamation-sign").show().fadeOut(1000);
				return;
			}

			//check duplicate item name -> update only quantity
			var duplicatedThing = app._Things.find(function(thing){
				return thing.get('title') == itemTitle && thing.get('username') == window.username;
			});

			if(duplicatedThing){
				duplicatedThing.save({
					qty: +duplicatedThing.get('qty') + +itemQty,
				});
			}else{
				//create dummy model without _id (waiting server for generating real _id)
				var ThingModelClient = app.Thing.extend({ noIoBind: true });
				var addingThing = new ThingModelClient({
					parent: window.faakId,
					title: itemTitle,
					qty: itemQty,
					description: itemDescription,
					username: window.username,
				});
				addingThing.save();
			}

			

			this.$("#inputTitle").val('').focus();
			this.$("#inputQty").val('1');
			this.$("#inputDescription").val('');
		},
		increaseQty: function(){
			var inputQty = this.$("#inputQty");
	    	inputQty.val(+inputQty.val() + 1);
	    },
	    decreaseQty: function(){
	    	var inputQty = this.$("#inputQty");
	    	var tmpQty = inputQty.val() - 1;
	    	inputQty.val(tmpQty < 1? 1 : tmpQty);
	    },
	    onKeyPressed: function(e){
	    	if ( e && e.which == ENTER_KEY ) {
				this.btnSaveClicked();
			}
	    },
	    onKeyDown: function(e){
	    	if( e && e.which == ARROW_UP_KEY){
				this.increaseQty();
				return false;
			}else if( e && e.which == ARROW_DOWN_KEY){
				this.decreaseQty();
				return false;
			}
	    }
	});

});