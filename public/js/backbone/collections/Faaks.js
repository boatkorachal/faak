var app = app || {};

(function() {
	'use strict';

	// Todo Collection
	// ---------------

	// The collection of todos is backed by *localStorage* instead of a remote
	// server.
	app.Faaks = Backbone.Collection.extend({
		url: 'faaks',
		model: app.Faak,
		socket: window.socket,
		initialize: function(){
			_.bindAll(this, 'serverCreate', 'collectionCleanup');
			this.ioBind('create', this.serverCreate, this);
		},
		serverCreate: function(data){

			var exists = this.get(data._id);
			if( ! exists){
				this.add(data);
			}else{
				data.fromServer = true;
				exists.set(data);
			}
		},
		collectionCleanup: function(callback){
			this.ioUnBindAll();
			this.each(function(model){
				model.modelCleanup();
			});
			return this;
		}
	});
	
}());
