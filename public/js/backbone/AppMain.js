var app = app || {};
app.page = 'main';
var ENTER_KEY = 13;
var ARROW_UP_KEY = 38;
var ARROW_DOWN_KEY = 40;
var socket = io.connect();
var username;
socket.on('connect', function(){
  socket.once('username', function(username){
    window.username = username;
    $(document).ready(function(){
		app._Faaks = app._Faaks || new app.Faaks();
		app._Things = app._Things || new app.Things({_id: '000000000000000000000000'});
		app._AppMainView = app._AppMainView || new app.AppMainView();

		app._Faaks.fetch();
		app._Things.fetch();
	});
  });
});

